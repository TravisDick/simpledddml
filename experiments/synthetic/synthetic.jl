using ArgParse

######################################
### Parsing Command Line Arguments ###
######################################

type Parameters
    dist_name::ASCIIString
    dim::Int
    k::Int
    p::Int
    l::Float64
    L::Float64
    n0::Int
    dispatch_size::Int
    chunk_size::Int
    train_chunks::Int
    test_chunks::Int
    seed::Int
    output::ASCIIString
    machine_file::ASCIIString
    num_workers::Int
    dispatch_mode::ASCIIString
end

function Parameters(ARGS)
    aps = ArgParseSettings()
    @add_arg_table aps begin
        "--dist_name"
            help = "Type of synthetic distribution"
            arg_type = ASCIIString
            default = "none"
        "--dimension", "-d"
            help = "Dimensionality of data"
            arg_type = Int
            required = true
        "--clusters", "-k"
            help = "Number of clusters"
            arg_type = Int
            required = true
        "--replication", "-p"
            help = "Replication factor"
            arg_type=Int
            default = 1
        "--n0", "-r"
            help = "Leaf-node size in random partition tree"
            arg_type = Int
            default=50
        "--dispatch_size", "-m"
            help = "Number of samples to use for dispatch and clustering"
            arg_type = Int
            required = true
        "--chunk_size", "-c"
            help = "Number of samples to be processed at a time."
            arg_type = Int
            default = 2000
        "--train_chunks"
            help = "Number of chunks to use for training"
            arg_type = Int
            required = true
        "--test_chunks"
            help = "Number of chunks to use for testing"
            arg_type = Int
            required=true
        "--seed"
            help = "Random seed"
            arg_type = Int
            default = 0
        "--output"
            help = "File to store the result in"
            arg_type = ASCIIString
            default = "./results.jld"
        "--workers"
            help = "Path to a machine file in the julia format"
            arg_type = ASCIIString
            default = ""
        "--num_workers"
            help = "Number of workers to use"
            arg_type = Int
            default = 1
        "--dispatch_mode"
            help = "Which dispatcher to use: random or kmpp"
            arg_type = ASCIIString
            default = "kmpp"
    end
    parsed_args = parse_args(ARGS, aps)

    dist_name = parsed_args["dist_name"]
    dim = parsed_args["dimension"]
    k = parsed_args["clusters"]
    p = parsed_args["replication"]
    l = p/2/k
    L = p*2/k
    n0 = parsed_args["n0"]
    dispatch_size = parsed_args["dispatch_size"]
    chunk_size = parsed_args["chunk_size"]
    train_chunks = parsed_args["train_chunks"]
    test_chunks = parsed_args["test_chunks"]
    seed = parsed_args["seed"]
    output = parsed_args["output"]
    machine_file = parsed_args["workers"]
    num_workers = parsed_args["num_workers"]
    num_workers = min(num_workers, k + 1)
    dispatch_mode = parsed_args["dispatch_mode"]

    return Parameters(dist_name, dim, k, p, l, L, n0, dispatch_size,
                      chunk_size, train_chunks, test_chunks,
                      seed, output, machine_file, num_workers, dispatch_mode)
end

const ps = Parameters(ARGS)

function make_subsample_iterator(ps::Parameters)
    σ = 200^(-1/ps.dim) / 3
    return SyntheticDataIterator(0, 200, σ, 30, ps.dim, ps.dispatch_size)
end

function make_train_chunk_iterator(ps::Parameters)
    σ = 200^(-1/ps.dim) / 3
    return SyntheticDataChunkIterator(0, 200, σ, 30, ps.dim, ps.chunk_size,
                                 ps.train_chunks)
end

function make_test_chunk_iterator(ps::Parameters)
    σ = 200^(-1/ps.dim) / 3
    return SyntheticDataChunkIterator(0, 200, σ, 30, ps.dim, ps.chunk_size,
                                 ps.test_chunks)
end

#######################################
### Spawn desired number of workers ###
#######################################

function parse_machine_file_line(line)
    line = strip(line)
    ix = findfirst(line, '*')
    if ix == 0
        return 1, line
    else
        mult = parse(Int, line[1:ix-1])
        host = line[ix+1:end]
        return mult, host
    end
end

function parse_machine_file(path)
    machines = Dict{ASCIIString, Int}()
    open(path, "r") do fh
        for line in eachline(fh)
            mult, host = parse_machine_file_line(line)
            machines[host] = mult
        end
    end
    return machines
end

function launch_workers(ps::Parameters)
    available_machines = parse_machine_file(ps.machine_file)
    total_available = sum(values(available_machines))
    @assert total_available >= ps.num_workers
    workers_to_launch = Dict{ASCIIString, Int}()
    remaining = ps.num_workers
    for k in cycle(keys(available_machines))
        if available_machines[k] > 0
            workers_to_launch[k] = get(workers_to_launch, k, 0) + 1
            available_machines[k] -= 1
            remaining -= 1
            if remaining <= 0
                break
            end
        end
    end
    println("Starting workers: ", workers_to_launch)
    addprocs([(host, num) for (host, num) in workers_to_launch])
end

if ps.num_workers > 1
    if ps.machine_file == ""
        println("Launching $(ps.num_workers) locally")
        addprocs(ps.num_workers)
    else
        launch_workers(ps)
    end
end

import JLD; @everywhere using JLD
import SimpleDDDML; @everywhere using SimpleDDDML

###############################
### Simple greedy scheduler ###
###############################

function schedule_jobs(do_job, workers, jobs)
    s = start(jobs)
    function next_job()
        (job, s) = next(jobs, s)
        return job
    end
    finished() = done(jobs, s)
    @sync for worker in workers
        @async while !finished()
            job = next_job()
            do_job(worker, job)
        end
    end
end

############################
### Subsampling the data ###
############################

function subsample(ps::Parameters)
    data_iterator = make_subsample_iterator(ps)
    X = Array(Float64, ps.dim, ps.dispatch_size)
    for (i, (x, y)) in enumerate(data_iterator)
        X[:,i] = x
    end
    return X
end

###################
### Dispatching ###
###################

@everywhere type DispatchBuffer
    destination::Int
    cluster_id::Int
    chunk_size::Int
    num_entries::Int
    dim::Int
    xs::Vector{Float64}
    ys::Vector{Int}
end

@everywhere function DispatchBuffer(dest, cluster, chunk_size, dim)
    xs = Array(Float64, chunk_size * dim)
    ys = Array(Int, chunk_size)
    return DispatchBuffer(dest, cluster, chunk_size, 0, dim, xs, ys)
end

@everywhere function send!(db::DispatchBuffer)
    if db.num_entries == 0
        return
    end
    remotecall_wait(db.destination, add_global_examples, db.cluster_id,
                    db.xs[1:db.num_entries*db.dim], db.ys[1:db.num_entries])
    db.num_entries = 0
end

@everywhere function add_example(db::DispatchBuffer, x::Vector, y::Int)
    x_start = db.num_entries*db.dim + 1
    x_end = (db.num_entries+1) * db.dim
    db.xs[x_start:x_end] = x
    y_ix = db.num_entries+1
    db.ys[y_ix] = y
    db.num_entries += 1
    if db.num_entries == db.chunk_size
        send!(db)
    end
end

@everywhere function init_dispatch_buffers(cs, dim)
    global global_dispatch_buffers = Dict{Int,DispatchBuffer}()
    ca = get_global_cluster_assignment()
    for k in cluster_keys(global_dispatcher)
        db = DispatchBuffer(ca[k], k, cs, dim)
        global_dispatch_buffers[k] = db
    end
end

@everywhere function get_global_dispatch_buffers()
    global global_dispatch_buffers
    global_dispatch_buffers
end

@everywhere function init_global_dispatcher(cache_file)
    try
        global global_dispatcher = load(cache_file, "dispatcher")
    catch e
        println(e)
    end
end

@everywhere function get_global_dispatcher()
    global global_dispatcher
    return global_dispatcher
end

function dispatcher_cache_filename(ps::Parameters)
    joinpath(dirname(ps.output), "dispatcher_$(hash(ps)).jld")
end

function cache_dispatcher(ps::Parameters, file)
    if ps.dispatch_mode == "kmpp"
        sX = subsample(ps)
        l = floor(Int, ps.l * size(sX, 2))
        L = ceil(Int, ps.L * size(sX, 2))
        clustering = kpmeans(sX, ps.k, ps.p, l, L)
        dispatcher = RPTDispatcher(sX, ps.n0, clustering)
    elseif ps.dispatch_mode == "random"
        dispatcher = RandomDispatcher(ps.k, ps.p)
    elseif ps.dispatch_mode == "bpt"
        sX = subsample(ps)
        dispatcher = PartitionTreeDispatcher(sX, ps.k)
    else
        error("Unrecognized dispatcher mode")
    end
    save(file, "dispatcher", dispatcher)
end

# Building the dispatcher
function make_dispatchers(ps::Parameters)
    cache_file = dispatcher_cache_filename(ps)
    if !isfile(cache_file)
        cache_dispatcher(ps, cache_file)
    end
    @sync for w in workers()
        @async remotecall_wait(init_global_dispatcher, w, cache_file)
    end
    dispatcher = load(cache_file, "dispatcher")
    return cluster_keys(dispatcher)
end

# A map indicating which worker is responsible for each worker
@everywhere function init_global_cluster_assignment(cluster_assignment)
    global global_cluster_assignment = cluster_assignment
end

@everywhere function get_global_cluster_assignment()
    global global_cluster_assignment
    return global_cluster_assignment
end

function make_cluster_assignment(keys)
    cluster_assignment = Dict{Int,Int}()
    for (w,k) in zip(cycle(workers()), keys)
        cluster_assignment[k] = w
    end
    @sync for w in workers()
        @async remotecall_wait(init_global_cluster_assignment, w, cluster_assignment)
    end
end

function make_dispatch_buffers(cs,dim)
    @sync for w in workers()
        @async remotecall_wait(init_dispatch_buffers, w, cs, dim)
    end
end

# Dispatching the training data
@everywhere function dispatch_data_worker(
        chunk,
        dispatcher = get_global_dispatcher(),
        dispatch_buffers = get_global_dispatch_buffers())

    for (x,y) in chunk
        for k in dispatch(dispatcher, x)
            add_example(dispatch_buffers[k], x, y)
        end
    end
end

@everywhere function dispatch_final_examples(
        dispatch_buffers = get_global_dispatch_buffers())
    for (k,db) in dispatch_buffers
        send!(db)
    end
end

function dispatch_data(ps::Parameters)
    chunks = make_train_chunk_iterator(ps)
    # schedule_jobs(workers(), chunks) do w, chunk
    #     remotecall_wait(dispatch_data_worker, w, chunk)
    # end
    pmap(dispatch_data_worker, chunks)
    @sync for w in workers()
        @async remotecall_wait(w, dispatch_final_examples)
    end
end

#############################
### Getting Cluster Sizes ###
#############################

@everywhere function report_cluster_sizes()
    global global_learners
    [ k => num_examples(l) for (k,l) in global_learners ]
end

function get_cluster_sizes()
    cluster_sizes = Dict{Int,Int}()
    for w in workers()
        sizes = remotecall_fetch(report_cluster_sizes, w)
        for (k,s) in sizes
            cluster_sizes[k] = s
        end
    end
    cluster_sizes
end

################
### Learning ###
################

@everywhere function init_global_learners(dim, max_examples)
    global global_cluster_assignment
    global global_learners = Dict{Int, NLOptLearner}()
    me = myid()
    if ispath("/mnt/ssd/tdick")
        base_path = "/mnt/ssd/tdick"
    elseif ispath("/scratch/ubuntu")
        base_path = "/scratch/ubuntu"
    else
        base_path = "./"
    end
    for (k, w) in global_cluster_assignment
        if w == me
            global_learners[k] = NLOptLearner(base_path, dim, 30, 0.1)
        end
    end
end

# Initializing the workers
function make_learners(ps::Parameters)
    dim = ps.dim
    max_examples = ceil(Int, 1.01 * ps.train_chunks * ps.chunk_size * ps.L)
    @sync for w in workers()
        @async remotecall_wait(init_global_learners, w, dim, max_examples)
    end
end

@everywhere function add_global_examples(key::Int, Xs, ys)
    global global_learners
    add_examples!(global_learners[key], Xs, ys)
end

@everywhere function train_global_models()
    global global_learners
    for (k, l) in global_learners
        train!(l)
    end
end

# Training the models
function train_models()
    @sync for w in workers()
        @async remotecall_wait(train_global_models, w)
    end
end

###############
### Testing ###
###############

@everywhere function global_predict(k, x)
    global global_learners
    return predict(global_learners[k], x)
end

@everywhere function accuracy(x, y)
    global global_dispatcher
    global global_cluster_assignment
    keys = dispatch(global_dispatcher, x)
    yhats = [remotecall_fetch(global_predict, global_cluster_assignment[k], k, x)
             for k in keys]
    return sum(yhats .== y) / length(yhats)
end

@everywhere function test_models_worker(chunk)
    total_queries = 0
    total_score = 0.0
    for (x,y) in chunk
        total_queries += 1
        total_score += accuracy(x, y)
    end
    return total_queries, total_score
end

function test_models(ps::Parameters)
    chunks = make_test_chunk_iterator(ps)
    total_queries = 0
    total_score = 0.0
    schedule_jobs(workers(), chunks) do w, chunk
        q, s = remotecall_fetch(test_models_worker, w, chunk)
        total_queries += q
        total_score += s
    end
    return total_score / total_queries
end

######################
### Run Experiment ###
######################

function vtic(msg)
    print(msg, "...")
    tic()
end

function vtoc()
    time = toq()
    println("done. ($time sec.)")
    return time
end

function run_experiment(ps::Parameters)
    tic()

    vtic("Initializing dispatchers")
    keys = make_dispatchers(ps)
    make_cluster_assignment(keys)
    make_dispatch_buffers(ps.chunk_size, ps.dim)
    dispatch_init_time = vtoc()

    vtic("Initializing learners")
    learners = make_learners(ps)
    learner_init_time = vtoc()

    vtic("Dispatching data")
    dispatch_data(ps)
    dispatch_time = vtoc()

    cluster_sizes = get_cluster_sizes()
    ncs = length(cluster_sizes)
    min_size, max_size = extrema(values(cluster_sizes))
    println("[# Clusters = $ncs, min size = $min_size, max size = $max_size]")

    vtic("Training models")
    train_models()
    train_time = vtoc()

    vtic("Testing models")
    score = test_models(ps)
    test_time = vtoc()

    total_time = toq()

    println("Accuracy = ", score)
    return Dict("params" => ps,
                "score" => score,
                "dispatch_init_time" => dispatch_init_time,
                "learner_init_time" => learner_init_time,
                "dispatch_time" => dispatch_time,
                "train_time" => train_time,
                "test_time" => test_time,
                "total_time" => total_time,
                "cluster_sizes" => cluster_sizes,
                "num_clusters" => length(keys))
end

function main()
    # Set random seeds
    srand(ps.seed)
    for w in workers()
        remotecall(srand, w, rand(Int))
    end
    save(ps.output, run_experiment(ps))
end

main()
