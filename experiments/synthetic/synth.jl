using SimpleDDDML, JLD

const data_distribution = SyntheticDistribution(0, 200, 200^(-1/20)/3, 30, 20)

function vtic(message)
    tic()
    @printf("%s...", message)
end

function vtoq()
    duration = toq()
    @printf("done! (%.2f sec.)\n", duration)
    return duration
end

function run_experiment(k, p, train_gb, nfs_base, local_base;
                        m = 10000, c = 5000, test_c = 5000, test_gb = 1,
                        train_mode = :liblinear, dispatch_mode = :kpmeans)
    vtic("Initializing")
    initialize(k, p, floor(Int, m*p/k/2), ceil(Int, 2*m*p/k), m, nfs_base, local_base, train_mode, dispatch_mode)
    init_time = vtoq()

    vtic("Dispatching data")
    T = floor(Int, train_gb * (1024^3)  / (c * 21 * sizeof(Float64)))
    dispatch_data(c, T)
    dispatch_time = vtoq()

    vtic("Training models")
    println()
    train_models()
    train_time = vtoq()

    vtic("Testing models")
    test_T = floor(Int, test_gb * (1024^3)  / (test_c * 21 * sizeof(Float64)))
    acc = test_models(test_c, test_gb)
    test_time = vtoq()

    return Dict(
        :init_time => init_time,
        :dispatch_time => dispatch_time,
        :train_time => train_time,
        :test_time => test_time,
        :acc => acc,
        )
end

function schedule_jobs(do_job, workers, jobs)
    s = start(jobs)
    function next_job()
        (job, s) = next(jobs, s)
        return job
    end
    finished() = done(jobs, s)
    @sync for worker in workers
        @async while !finished()
            job = next_job()
            do_job(worker, job)
        end
    end
end

type WorkerAssignment
    dispatchers::Vector{Int}     # worker ids responsible for dispatching data
    learners::Vector{Int}        # worker ids responsible for learning
    c2w::Dict{Int, Int}          # which worker owns each cluster
    w2cs::Dict{Int, Vector{Int}} # which clusters does a worker own
end

WorkerAssignment() = WorkerAssignment(Array(Int, 0), Array(Int, 0),
                                      Dict{Int,Int}(), Dict{Int,Vector{Int}}())

function create_worker_assignment(cluster_keys)
    # Query each of the workers for their hostname
    hostnames = Array(ASCIIString, length(workers()) + 1)
    @sync for wid in workers()
        @async hostnames[wid] = remotecall_fetch(gethostname, wid)
    end
    # Get a map from hostnames to worker ids
    hosts = Dict{ASCIIString, Vector{Int}}()
    for wid in workers()
        if !haskey(hosts, hostnames[wid])
            hosts[hostnames[wid]] = Array(Int, 0);
        end
        push!(hosts[hostnames[wid]], wid)
    end
    # Create worker assignment
    wa = WorkerAssignment()
    # Divide the workers in half between dispatchers and learners (per host).
    for (host, wids) in hosts
        n = length(wids)
        d = div(n, 2)
        append!(wa.dispatchers, wids[1:d])
        append!(wa.learners, wids[d+1:end])
    end
    # Now cycle through the learners assigning them the different cluster keys
    for (wid, key) in zip(cycle(wa.learners), cluster_keys)
        wa.c2w[key] = wid
        if !haskey(wa.w2cs, wid)
            wa.w2cs[wid] = Array(Int, 0)
        end
        push!(wa.w2cs[wid], key)
    end
    return wa
end

function set_worker_assignment(wa)
    global g_worker_assignment = wa
end

function get_worker_assignment()
    global g_worker_assignment
    return g_worker_assignment
end

function initialize(k, p, l, L, m, nfs_base, local_base, train_mode, dispatch_mode)
    # Create the dispatcher
    dispatcher = create_dispatcher(k, p, l, L, m, dispatch_mode)
    # Create a worker assignment and distribute it
    wa = create_worker_assignment(cluster_keys(dispatcher))
    set_worker_assignment(wa)
    @sync for wid in workers()
        @async remotecall_wait(set_worker_assignment, wid, wa)
    end
    # Save the dispatcher and load it on all dispatchers
    dcachefile = joinpath(nfs_base, "d$(hash(rand())).jld")
    save_dispatcher_cache(dcachefile, dispatcher)
    @sync for wid in wa.dispatchers
        @async remotecall_wait(load_dispatcher_cache, wid, dcachefile)
    end
    # Initialize the learners
    @sync for wid in wa.learners
        @async remotecall_wait(init_learners, wid, local_base, train_mode)
    end
end

function dispatch_data(chunk_size, num_chunks)
    wa = get_worker_assignment()
    pmap(dispatch_chunk, repeated(chunk_size, num_chunks);
         err_retry=true, pids=wa.dispatchers)
    # schedule_jobs(wa.dispatchers, 1:num_chunks) do wid, i
    #     remotecall_wait(dispatch_chunk, wid, chunk_size)
    # end
end

function train_models()
    wa = get_worker_assignment()
    @sync for (i, wid) in enumerate(wa.learners)
        @async remotecall_wait(train!, wid)
    end
end

function test_models(chunk_size, num_chunks)
    wa = get_worker_assignment()
    total_accuracy = 0.0
    function add_accuracy(acc)
        total_accuracy += acc
    end
    schedule_jobs(wa.dispatchers, 1:num_chunks) do wid, i
        add_accuracy(remotecall_fetch(test_chunk, wid, chunk_size))
    end
    return total_accuracy / (chunk_size * num_chunks)
end

### Dispatchers ###

function get_dispatcher()
    global g_dispatcher
    return g_dispatcher
end

function load_dispatcher_cache(cache_file)
    global g_dispatcher = load(cache_file)["dispatcher"]
end

function save_dispatcher_cache(cache_file, d)
    save(cache_file, "dispatcher", d)
end

function create_dispatcher(k, p, l, L, size, dispatch_mode)
    if dispatch_mode == :kmpp
        X = Array(Float64, data_distribution.dim, size)
        for i in 1:size
            X[:,i], y = sample(data_distribution)
        end
        clustering = kpmeans(X, k, p, l, L)
        dispatcher = RPTDispatcher(X, 50, clustering)
    elseif dispatch_mode == :random
        dispatcher = RandomDispatcher(k, p)
    end
    return dispatcher
end

function dispatch_chunk(chunk_size, timeout = 10.0)
    wa = get_worker_assignment()
    d = get_dispatcher()
    worker_X = Dict{Int, Vector{Float64}}()
    worker_y = Dict{Int, Vector{Int}}()
    for i in 1:chunk_size
        x, y = sample(data_distribution)
        for k in dispatch(d, x)
            if !haskey(worker_X, k)
                worker_X[k] = Array(Float64, 0)
                worker_y[k] = Array(Int, 0)
            end
            append!(worker_X[k], x)
            push!(worker_y[k], y)
        end
    end

    for (wid, ks) in wa.w2cs
        examples = Dict{Int, Tuple{Vector{Float64}, Vector{Int}}}()
        for k in ks
            if haskey(worker_X, k)
                examples[k] = (worker_X[k], worker_y[k])
            end
        end
        remotecall_wait(add_examples!, wid, examples)
    end
end

function test_chunk(chunk_size)
    wa = get_worker_assignment()
    d = get_dispatcher()
    total_accuracy = 0.0
    for i in 1:chunk_size
        x, y = sample(data_distribution)
        count = 0
        acc = 0.0
        for k in dispatch(d, x)
            yhat = remotecall_fetch(predict, wa.c2w[k], k, x)
            count += 1
            if yhat == y
                acc += 1
            end
        end
        total_accuracy += (acc / count)
    end
    return total_accuracy
end

### Learners ###

function get_learners()
    global g_learners
    return g_learners
end

function init_learners(local_base, train_mode)
    wa = get_worker_assignment()
    global g_learners = Dict{Int, DiskLearner}()
    if haskey(wa.w2cs, myid())
        for k in wa.w2cs[myid()]
            g_learners[k] = DiskLearner(local_base, data_distribution.dim, 30; train_mode = train_mode)
        end
    end
end

import SimpleDDDML: add_examples!, train!, predict

add_examples!(k::Int, Xs, ys) = add_examples!(get_learners()[k], Xs, ys)

function add_examples!(examples::Dict{Int, Tuple{Vector{Float64}, Vector{Int}}})
    for (k, (X, y)) in examples
        add_examples!(k, X, y)
    end
end

function train!()
    for (k, l) in get_learners()
        train_time = @elapsed train!(l)
        gc()
        @printf("Finished training model for cluster %d (%.2f sec)\n", k, train_time)
    end
end

predict(k::Int, x) = predict(get_learners()[k], x)
