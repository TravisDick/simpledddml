using ArgParse

######################################
### Parsing Command Line Arguments ###
######################################

type Parameters
    train_path::ASCIIString
    test_path::ASCIIString
    dim::Int
    k::Int
    p::Int
    l::Float64
    L::Float64
    n0::Int
    train_size::Int
    dispatch_frac::Float64
    chunk_size::Int
    seed::Int
    output::ASCIIString
    machine_file::ASCIIString
    num_workers::Int
end

function Parameters(ARGS)
    aps = ArgParseSettings()
    @add_arg_table aps begin
        "--base_directory", "-b"
            help = "Path to parent of train.txt and test.txt"
            required = true
        "--dimension", "-d"
            help = "Dimensionality of data"
            arg_type = Int
            required = true
        "--clusters", "-k"
            help = "Number of clusters"
            arg_type = Int
            required = true
        "--replication", "-p"
            help = "Replication factor"
            arg_type=Int
            default = 1
        "--n0", "-r"
            help = "Leaf-node size in random partition tree"
            arg_type = Int
            default=50
        "--train_size", "-n"
            help = "Number of training examples"
            arg_type = Int
            required = true
        "--dispatch_size", "-m"
            help = "Number of samples to use for dispatch and clustering"
            arg_type = Int
            required = true
        "--chunk_size", "-c"
            help = "Number of samples to be processed at a time."
            arg_type = Int
            default = 2000
        "--seed"
            help = "Random seed"
            arg_type = Int
            default = 0
        "--output"
            help = "File to store the result in"
            arg_type = ASCIIString
            default = "results.jld"
        "--workers"
            help = "Path to a machine file in the julia format"
            arg_type = ASCIIString
            default = ""
        "--num_workers"
            help = "Number of workers to use"
            arg_type = Int
            default = 1
    end
    parsed_args = parse_args(ARGS, aps)

    base_directory = parsed_args["base_directory"]
    train_path = joinpath(base_directory, "train.txt")
    test_path = joinpath(base_directory, "test.txt")
    dim = parsed_args["dimension"]
    k = parsed_args["clusters"]
    p = parsed_args["replication"]
    l = p/2/k
    L = p*2/k
    n0 = parsed_args["n0"]
    train_size = parsed_args["train_size"]
    dispatch_frac = parsed_args["dispatch_size"] / train_size
    chunk_size = parsed_args["chunk_size"]
    seed = parsed_args["seed"]
    output = parsed_args["output"]
    machine_file = parsed_args["workers"]
    num_workers = min(k+1, parsed_args["num_workers"])

    return Parameters(train_path, test_path, dim, k, p, l, L, n0, train_size,
                      dispatch_frac, chunk_size, seed, output,
                      machine_file, num_workers)
end

const ps = Parameters(ARGS)

#######################################
### Spawn desired number of workers ###
#######################################

function parse_machine_file_line(line)
    line = strip(line)
    ix = findfirst(line, '*')
    if ix == 0
        return 1, line
    else
        mult = parse(Int, line[1:ix-1])
        host = line[ix+1:end]
        return mult, host
    end
end

function parse_machine_file(path)
    machines = Dict{ASCIIString, Int}()
    open(path, "r") do fh
        for line in eachline(fh)
            mult, host = parse_machine_file_line(line)
            machines[host] = mult
        end
    end
    return machines
end

function launch_workers(ps::Parameters)
    available_machines = parse_machine_file(ps.machine_file)
    total_available = sum(values(available_machines))
    @assert total_available >= ps.num_workers
    workers_to_launch = Dict{ASCIIString, Int}()
    remaining = ps.num_workers
    for k in cycle(keys(available_machines))
        if available_machines[k] > 0
            workers_to_launch[k] = get(workers_to_launch, k, 0) + 1
            available_machines[k] -= 1
            remaining -= 1
            if remaining <= 0
                break
            end
        end
    end
    println("Starting workers: ", workers_to_launch)
    addprocs([(host, num) for (host, num) in workers_to_launch])
end

if ps.num_workers > 1
    if ps.machine_file == ""
        println("Launching $(ps.num_workers) locally")
        addprocs(ps.num_workers)
    else
        launch_workers(ps)
    end
end

import JLD; @everywhere using JLD
import SimpleDDDML; @everywhere using SimpleDDDML


###############################
### Simple greedy scheduler ###
###############################

function schedule_jobs(do_job, workers, jobs)
    s = start(jobs)
    function next_job()
        (job, s) = next(jobs, s)
        return job
    end
    finished() = done(jobs, s)
    @sync for worker in workers
        @async while !finished()
            job = next_job()
            do_job(worker, job)
        end
    end
end

############################
### Subsampling the data ###
############################

@everywhere function subsample_worker(chunk)
    dim = get_dimension(chunk)
    Xs = Array(Float64, 0)
    for (x,y) in chunk
        append!(Xs, x)
    end
    return reshape(Xs, dim, div(length(Xs), dim))
end

function subsample(ps::Parameters)
    chunks = LibSVMChunkIterator(ps.train_path, ps.dim, ps.chunk_size;
                                 read_prob = ps.dispatch_frac)
    X = hcat(pmap(subsample_worker, chunks)...)
    m = floor(Int, ps.dispatch_frac * ps.train_size)
    output_size = min(m, size(X,2))
    return X[:,1:output_size]
end

###################
### Dispatching ###
###################

@everywhere function init_global_dispatcher(cache_file)
    try
        global global_dispatcher = load(cache_file, "dispatcher")
    catch e
        println(e)
    end
end

@everywhere function get_global_dispatcher()
    global global_dispatcher
    return global_dispatcher
end

function dispatcher_cache_filename(ps::Parameters)
    joinpath(expanduser("~/dispatchers"), "$(hash(ps)).jld")
end

function cache_dispatcher(ps::Parameters, file)
    sX = subsample(ps)
    l = floor(Int, ps.l * size(sX, 2))
    L = ceil(Int, ps.L * size(sX, 2))
    clustering = lpr_cluster(sX, ps.k, ps.p, l, L)
    dispatcher = RPTDispatcher(sX, ps.n0, clustering)
    save(file, "dispatcher", dispatcher)
end

# Building the dispatcher
function make_dispatchers(ps::Parameters)
    cache_file = dispatcher_cache_filename(ps)
    if !isfile(cache_file)
        cache_dispatcher(ps, cache_file)
    end
    @sync for w in workers()
        @async remotecall_wait(init_global_dispatcher, w, cache_file)
    end
    dispatcher = load(cache_file, "dispatcher")
    return cluster_keys(dispatcher)
end

# A map indicating which worker is responsible for each worker
@everywhere function init_global_cluster_assignment(cluster_assignment)
    global global_cluster_assignment = cluster_assignment
end

@everywhere function get_global_cluster_assignment()
    global global_cluster_assignment
    return global_cluster_assignment
end

function make_cluster_assignment(keys)
    cluster_assignment = Dict{Int,Int}()
    for (w,k) in zip(cycle(workers()), keys)
        cluster_assignment[k] = w
    end
    @sync for w in workers()
        @async remotecall_wait(init_global_cluster_assignment, w, cluster_assignment)
    end
end

# Dispatching the training data
@everywhere function dispatch_data_worker(
        chunk,
        dispatcher = get_global_dispatcher(),
        cluster_assignment = get_global_cluster_assignment())
    Xs = [k => Array(Float64, 0) for k in cluster_keys(dispatcher)]
    ys = [k => Array(Int, 0) for k in cluster_keys(dispatcher)]
    # Dispatch the data locally
    for (x,y) in chunk
        for k in dispatch(dispatcher, x)
            append!(Xs[k], x)
            push!(ys[k], y)
        end
    end
    # Send it to the learners
    for k in keys(Xs)
        X = Xs[k]
        y = ys[k]
        if length(X) > 0
            w = cluster_assignment[k]
            remotecall_wait(add_global_examples, w, k, X, y)
        end
    end
end

function dispatch_data(ps::Parameters)
    chunks = LibSVMChunkIterator(ps.train_path, ps.dim, ps.chunk_size)
    schedule_jobs(workers(), chunks) do w, chunk
        remotecall_wait(dispatch_data_worker, w, chunk)
    end
end

################
### Learning ###
################

@everywhere function init_global_learners(dim, max_examples)
    global global_cluster_assignment
    global global_learners = Dict{Int, Learner{LibLinClassifier}}()
    me = myid()
    for (k, w) in global_cluster_assignment
        if w == me
            global_learners[k] = Learner(dim, max_examples)
        end
    end
end

# Initializing the workers
function make_learners(ps::Parameters)
    dim = ps.dim
    max_examples = ceil(Int, 1.01 * ps.train_size * ps.L)
    @sync for w in workers()
        @async remotecall_wait(init_global_learners, w, dim, max_examples)
    end
end

@everywhere function add_global_examples(key::Int, Xs, ys)
    global global_learners
    add_examples!(global_learners[key], Xs, ys)
end

@everywhere function train_global_models()
    global global_learners
    for (k, l) in global_learners
        train!(l)
    end
end

# Training the models
function train_models()
    @sync for w in workers()
        @async remotecall_wait(train_global_models, w)
    end
end

###############
### Testing ###
###############

@everywhere function global_predict(k, x)
    global global_learners
    return predict(global_learners[k], x)
end

@everywhere function accuracy(x, y)
    global global_dispatcher
    global global_cluster_assignment
    keys = dispatch(global_dispatcher, x)
    yhats = [remotecall_fetch(global_predict, global_cluster_assignment[k], k, x)
             for k in keys]
    return sum(yhats .== y) / length(yhats)
end

@everywhere function test_models_worker(chunk)
    total_queries = 0
    total_score = 0.0
    for (x,y) in chunk
        total_queries += 1
        total_score += accuracy(x, y)
    end
    return total_queries, total_score
end

function test_models(ps::Parameters)
    chunks = LibSVMChunkIterator(ps.test_path, ps.dim, 500)
    total_queries = 0
    total_score = 0.0
    schedule_jobs(workers(), chunks) do w, chunk
        q, s = remotecall_fetch(test_models_worker, w, chunk)
        total_queries += q
        total_score += s
    end
    return total_score / total_queries
end

######################
### Run Experiment ###
######################

function vtic(msg)
    print(msg, "...")
    tic()
end

function vtoc()
    time = toq()
    println("done. ($time sec.)")
    return time
end

function run_experiment(ps::Parameters)
    tic()

    vtic("Initializing dispatchers")
    keys = make_dispatchers(ps)
    make_cluster_assignment(keys)
    dispatch_init_time = vtoc()

    vtic("Initializing learners")
    learners = make_learners(ps)
    learner_init_time = vtoc()

    vtic("Dispatching data")
    dispatch_data(ps)
    dispatch_time = vtoc()

    vtic("Training models")
    train_models()
    train_time = vtoc()

    vtic("Testing models")
    score = test_models(ps)
    test_time = vtoc()

    total_time = toq()

    println("Accuracy = ", score)
    return Dict("params" => ps,
                "score" => score,
                "dispatch_init_time" => dispatch_init_time,
                "learner_init_time" => learner_init_time,
                "dispatch_time" => dispatch_time,
                "train_time" => train_time,
                "test_time" => test_time,
                "total_time" => total_time,
                "num_clusters" => length(keys))
end

function main()
    # Set random seeds
    srand(ps.seed)
    for w in workers()
        remotecall(srand, w, rand(Int))
    end
    save(ps.output, run_experiment(ps))
end

main()
