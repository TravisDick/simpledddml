using JLD, PyPlot

function main()
    results = load("results.jld")
    for method in ["nn", "rand"]
        for name in ["mnist8m", "cifar10_in3c", "cifar10_in4d"]
            label = "$(name)_$(method)"
            plot(results["ks"], results[label], "-x", label=label)
        end
    end
    semilogx()
    xlim([minimum(results["ks"]), maximum(results["ks"])])
    xticks(results["ks"], results["ks"])
    xlabel("Number of clusters")
    ylabel("Average per-cluster class entropy")
    legend()
    savefig("class_entropy.pdf")
end
main()
