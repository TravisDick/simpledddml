import SimpleDDDML

@everywhere begin

using SimpleDDDML

"Comptues the entropy of a probability vector `p`."
function entropy(ps::Vector)
    result = 0.0
    for p in ps
        if p > 0
            result -= p * log(p)
        end
    end
    result
end

"Computes the average entropy of the columns of `A`."
mean_entropy(A::Array) = mean([entropy(A[:,i]) for i in 1:size(A,2)])

"""
Uses dispatcher `d` to dispatch the data given by `(X,y)`. Returns the mean
entropy the the empirical class distribution across clusters.
"""
function dispatcher_class_entropy(d, X, y, num_classes)
    dkeys = cluster_keys(d)
    probs = zeros(num_classes, length(dkeys))
    cluster_to_ix = [key => i for (i,key) in enumerate(dkeys)]
    for i in 1:size(X, 2)
        for key in dispatch(d, slice(X,:,i))
            probs[y[i]+1, cluster_to_ix[key]] += 1
        end
    end
    for i in 1:size(probs,2)
        probs[:,i] /= sum(probs[:,i])
    end
    mean_entropy(probs)
end

"""
Draws a sample of `tn` points from the `dim`-dimensional dataset pointed to by
`path`. For each value of `k` in `ks`, a new dispatcher with `k` clusters is
constructed. When `kind == :nndispatch` then the dispatcher is an
`RPTDispatcher` constructed from a second iid sample of `dn` points from the
dataset. When `kind == :randomdispatch` the dispatcher is set to
`RandomDispatcher(k,1)`. Returns a vector of the same size as `k` which contains
the mean class-distribution entropies for the respective dispatchers.
"""
function k_to_entropy_sample(path, dn, tn, dim, num_classes, ks, kind)
    X, y = subsample_libsvm_file(path, tn, dim)
    if kind == :nn
        dX, dy = subsample_libsvm_file(path, dn, dim)
    end
    function sample_k(k)
        if kind == :nn
            clustering = kpmeans(dX, k, 1, floor(Int, dn/2/k), ceil(Int, dn*2/k))
            dispatcher = RPTDispatcher(dX, 50, clustering)
        elseif kind == :rand
            dispatcher = RandomDispatcher(k, 1)
        else
            error("Unrecognized dispatcher kind $kind")
        end
        dispatcher_class_entropy(dispatcher, X, y, num_classes)
    end
    [sample_k(k) for k in ks]
end

"Computes the mean of `num_runs` runs of `k_to_entropy_sample`"
function k_to_entropy_mean(path, dn, tn, dim, num_classes, ks, kind, num_runs)
    total = @parallel (+) for i in 1:num_runs
        k_to_entropy_sample(path, dn, tn, dim, num_classes, ks, kind)
    end
    return total / num_runs
end

end

using JLD

function main()
    names = ["cifar10_in4d", "cifar10_in3c", "mnist8m"]
    dims = Dict("cifar10_in4d" => 144,
            "cifar10_in3c" => 160,
            "mnist8m" => 784)
    ks = [2^i for i in 1:10]
    jldopen("results.jld", "w") do fh
        write(fh, "ks", ks)
        for name in names
            tic()
            print("Running $name...")
            path = "../data/$name/train.txt"
            dn = 10000
            tn = 100000
            dim = dims[name]
            nc = 10
            nn_result = k_to_entropy_mean(path, dn, tn, dim, nc, ks, :nn, 10)
            rand_result = k_to_entropy_mean(path, dn, tn, dim, nc, ks, :rand, 10)
            write(fh, "$(name)_nn", nn_result)
            write(fh, "$(name)_rand", rand_result)
            println("done. ($(toq()) sec)")
        end
    end
end
main()
