function random_split(X)
    dim = rand(1:size(X,1))
    t = median(squeeze(X[dim, :],1))
    return dim, t
end

abstract PartitionNode

type PartitionInternal <: PartitionNode
    dim::Int
    t::Float64
    left::PartitionNode
    right::PartitionNode
end

type PartitionLeaf <: PartitionNode
end

function make_partition_tree(X, k)
    @assert ispow2(k)
    if k == 1
        return PartitionLeaf()
    else
        d, t = random_split(X)
        left_X = X[:, X[d,:] .<= t]
        right_X = X[:, X[d,:] .> t]
        left_tree = make_partition_tree(left_X, k>>1)
        right_tree = make_partition_tree(right_X, k>>1)
        return PartitionInternal(d, t, left_tree, right_tree)
    end
end

route(x::Vector, t::PartitionNode) = route(x, t, 0)
route(x::Vector, t::PartitionLeaf, ix::Int) = ix
route(x::Vector, t::PartitionInternal, ix::Int) = x[t.dim] <= t.t ? route(x, t.left, ix<<1|0) : route(x, t.right, ix<<1|1)

# Wrap the above data structure in a dispatcher type.

type PartitionTreeDispatcher
    t::PartitionNode
    k::Int
end

function PartitionTreeDispatcher(X::Matrix, k::Int)
    t = make_partition_tree(X, k)
    return PartitionTreeDispatcher(t, k)
end

cluster_keys(d::PartitionTreeDispatcher) = 0:(d.k-1)

dispatch(d::PartitionTreeDispatcher, x) = route(x, d.t)
