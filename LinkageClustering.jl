using Distances

abstract BinaryTree{T}

type BinaryTreeInternal{T} <: BinaryTree{T}
    left::BinaryTree{T}
    right::BinaryTree{T}
end

type BinaryTreeLeaf{T} <: BinaryTree{T}
    v::T
end

depth(t::BinaryTreeLeaf) = 0
depth(t::BinaryTreeInternal) = 1 + max(depth(t.left), depth(t.right))

treenumerate{T}(t::BinaryTreeLeaf{T}, f) = f(t)
function treenumerate{T}(t::BinaryTreeInternal{T}, f)
    treenumerate(t.left, f)
    treenumerate(t.right, f)
end

numleaves(::BinaryTreeLeaf) = 1
numleaves(t::BinaryTreeInternal) = numleaves(t.left) + numleaves(t.right)

typealias LinkageTree BinaryTree{Int}
typealias LinkageInternal BinaryTreeInternal{Int}
typealias LinkageLeaf BinaryTreeLeaf{Int}

function max_dist(D, a::LinkageLeaf, b::LinkageLeaf)
     return D[a.v, b.v]
end

function max_dist(D, a::LinkageInternal, b::LinkageLeaf)
    return max(max_dist(D, a.left, b), max_dist(D, a.right, b))
end

max_dist(D, a::LinkageLeaf, b::LinkageInternal) = max_dist(D, b, a)

function max_dist(D, a::LinkageInternal, b::LinkageInternal)
    return max(max_dist(D, a.left, b.left),
               max_dist(D, a.left, b.right),
               max_dist(D, a.right, b.left),
               max_dist(D, a.right, b.right))
end

function find_min_max_dist(D, nodes::Dict{Int, LinkageTree})
    best_pair = (0,0)
    best_d = Inf
    for (i, a) in nodes
        for (j, b) in nodes
            if i != j
                d = max_dist(D, a, b)
                if d < best_d
                    best_d = d
                    best_pair = (i,j)
                end
            end
        end
    end
    return best_pair
end

function complete_linkage(X)
    D = pairwise(SqEuclidean(), X)
    nodes = Dict{Int, LinkageTree}()
    for i in 1:size(X,2); nodes[i] = LinkageLeaf(i); end
    while length(nodes) > 1
        i, j = find_min_max_dist(D, nodes)
        parent = LinkageInternal(nodes[i], nodes[j])
        nodes[i] = parent
        delete!(nodes, j)
    end
    return first(values(nodes))
end

using Distances

### Distance functions for SingleLinkage nodes

function min_dist(D, a::LinkageLeaf, b::LinkageLeaf)
    return D[a.index, b.index]
end

function min_dist(D, a::LinkageInternal, b::LinkageLeaf)
    return min(min_dist(D, a.left, b), min_dist(D, a.right, b))
end

min_dist(D, a::LinkageLeaf, b::LinkageInternal) = dist(D, b, a)

function min_dist(D, a::LinkageInternal, b::LinkageInternal)
    return min(min_dist(D, a.left, b.left),
               min_dist(D, a.left, b.right),
               min_dist(D, a.right, b.left),
               min_dist(D, a.right, b.right))
end

### Implementation of single linkage

function find_min_min_dist(closest)
    best_pair = (0,0)
    best_dist = Inf
    for (i, (d, j)) in closest
        if d < best_dist
            best_pair = (i,j)
            best_dist = d
        end
    end
    return best_pair
end

function find_closest(D, nodes, i)
    best_dist = Inf
    best_j = 0
    for (j, node) in nodes
        if i != j
            d = dist(D, nodes[i], nodes[j])
            if d < best_dist
                best_dist = d
                best_j = j
            end
        end
    end
    return (best_dist, best_j)
end

function single_linkage(X)
    d, n = size(X)
    D = pairwise(SqEuclidean(), X) # matrix of distances

    nodes = Dict{Int, LinkageNode}()
    for i in 1:n; nodes[i] = LinkageLeaf(i); end
    closest = [i => find_closest(D, nodes, i) for i in 1:n]

    while length(nodes) > 1
        (i,j) = find_min_min_dist(closest)

        new_node = LinkageInternal(nodes[i], nodes[j])
        nodes[i] = new_node

        delete!(nodes, j)
        delete!(closest, j)
        # Everyone that was closest to nodes[j] is now closest to nodes[i]
        for (i′, (d, j′)) in closest
            if j′ == j
                closest[i′] = (d, i)
            end
        end
        # Find the closest neighbor for the new node
        closest[i] = find_closest(D, nodes, i)
    end
    return first(values(nodes))
end
