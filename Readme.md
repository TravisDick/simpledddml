# What is DDDML?

This project provides a simple and easily extendible implementation of the
data-dependent resource allocation paradigm proposed in [insert paper link
here!]. The basic idea is that when we distribute large datasets throughout a
cluster of worker machines, we should send similar data points to the same
worker, while still ensuring that the worker machines receive roughly balanced
quantities of data and that several copies of each data point are made for fault
tolerance. For more information, please consult the accompanying paper.

# Interfaces

In this section we describe the interfaces used in the `DDDML` package.

### `DataIterators`

`DataIterator`s are responsible for getting your data into julia. They must
implement the `Iterator` interface with `eltype(di::DataIterator) = (X,Y)`,
where `X` is the type of a feature vector and `Y` is the label type.

*Note: `DataIterator`s are often serialized and sent over the network. Therefore, they should be relatively small and save to serialize. For example, they might contain the path to the dataset on a networked file system together with an offset into that file.*

### `ChunkIterators`

In order to process a dataset efficiently in parallel, it should be possible for
multiple workers to each work on a subset of the data. A `ChunkIterator` must
implement the `Iterator` interface with `eltype(ci::ChunkIterator) =
DataIterator`. The idea is that a `ChunkIterator` partitions a given dataset
into pieces (each represented by a separate `DataIterator`) that may be
processed in parallel by different workers.

### `Dispatchers`

Dispatchers are responsible for partitioning the data among the worker machines.
The set of data that gets sent to one machine is referred to as a cluster. Any
dispatcher must implement the following interface:

- `cluster_keys(d::Dispatcher)` should return an iterable collection of the keys used to identify the data clusters. The keys could be the integers `1` through `k`, hashes, symbols, etc.
- `dispatch(d::Dispatcher, x)` returns an iterable collection of keys indicating which clusters the data point `x` should be sent to.

It is up to the dispatcher to ensure that each cluster receives similar data
points, that the clusters are comparable in size, and that each point gets
mapped to several clusters for fault tolerance.
