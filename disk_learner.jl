using NLopt
import LIBLINEAR

"A learner that saves its data to disk and only loads it while training."
type DiskLearner
    path::ASCIIString
    fh::IOStream
    dim::Int
    num_examples::Int
    λ::Float64
    model::Array{Float64, 2}
    maxeval::Int
    ftol::Float64
    xtol::Float64
    train_mode::Symbol
end

function DiskLearner(base_dir, dim, L, λ=0.1; maxeval=30, ftol=1e-5, xtol=1e-5,
                     train_mode = :liblinear)
    path = joinpath(base_dir, "$(hash(rand())).dat")
    fh = open(path, "w+")
    return DiskLearner(path, fh, dim, 0, λ, Array(Float64, dim+1, L),
                       maxeval, ftol, xtol, train_mode)
end

function add_example!(l::DiskLearner, x, y)
    write(l.fh, y)
    write(l.fh, x)
    l.num_examples += 1
end

function add_examples!(l::DiskLearner, xs, ys)
    n = length(ys)
    X = reshape(xs, l.dim, n)
    for i in 1:n
        write(l.fh, ys[i])
        write(l.fh, X[:,i])
    end
    l.num_examples += n
end

function get_data(l::DiskLearner)
    X = Array(Float64, l.dim, l.num_examples)
    y = Array(Int, l.num_examples)
    seekstart(l.fh)
    for i in 1:l.num_examples
        y[i] = read(l.fh, Int)
        for j in 1:l.dim
            X[j,i] = read(l.fh, Float64)
        end
    end
    seekend(l.fh)
    return X, y
end

function dot_with_bias(w, x)
    dotp = 0.
    for i in 1:length(x)
        dotp += w[i] * x[i]
    end
    dotp += w[end]
    return dotp
end

function train!(l::DiskLearner)
    if l.train_mode == :liblinear
        train_liblinear!(l)
    else
        train_lbfgs!(l)
    end
end

function train_lbfgs!(l::DiskLearner)
    X, y = get_data(l)
    # function value and gradient
    function fg!(w::Vector, g::Vector, pos_label::Int)
        fval = 0.0
        if (length(g) > 0) # compute both gradient and function val
            fill!(g, 0.0)
            b = 0.0
            for i in 1:l.num_examples
                bin_y = y[i] == pos_label ? 1.0 : -1.0
                b = 1.0 - bin_y * dot_with_bias(w, X[:,i])
                if (b > 0)
                    fval += b^2
                    for j in 1:l.dim
                        g[j] += -2.0 * bin_y * b * X[j,i]
                    end
                    g[end] += -2. * bin_y * b
                end
            end
            for i in 1:l.dim
                fval += l.λ*w[i]*w[i]
                g[i] += 2*l.λ*w[i]
            end
        else # compute only function val
            for i in 1:l.num_examples
                bin_y = y[i] == pos_label ? 1.0 : -1.0
                fval += max(0, 1 - bin_y * dot_with_bias(w, X[:,i]))^2
            end
            for i in 1:l.dim
                fval += l.λ*w[i]*w[i]
            end
        end
        return fval
    end
    for i in 1:size(l.model, 2)
        opt = Opt(:LD_LBFGS, l.dim+1)
        ftol_abs!(opt, l.ftol)
        xtol_abs!(opt, l.xtol)
        maxeval!(opt, l.maxeval)
        fgi!(w,g) = fg!(w, g, i)
        min_objective!(opt, fgi!)
        (min_f,min_w,ret) = optimize(opt, rand(l.dim + 1))
        l.model[:,i] = min_w
    end
end

function train_liblinear!(l::DiskLearner)
    X, y = get_data(l)
    m = LIBLINEAR.linear_train(y, X; solver_type = Cint(2), bias = 1.0)
    fill!(l.model, 0.0)
    w = reshape(m.w, m.nr_class, m.nr_feature+1)
    for i in 1:m.nr_class
        l.model[:, m.labels[i]] = w[i,:]
    end
end

function predict(l::DiskLearner, x)
    best_score = dot_with_bias(l.model[:,1], x)
    best_label = 1
    for i in 2:size(l.model, 2)
        score = dot_with_bias(l.model[:, i], x)
        if score > best_score
            best_score = score
            best_label = i
        end
    end
    return best_label
end
