__precompile__()
module SimpleDDDML

# Re-export the relevant clustering functions and types from ClusteringTools
using ClusteringTools
import ClusteringTools: cluster_keys
export exact_clustering, rounded_clustering, kpmeans
export ClusterSol, cluster_keys, num_clusters, num_points
export get_cluster, get_centers, objective_value

include("utils.jl")

include("libsvm_parse.jl")
export LibSVMFileIterator, LibSVMChunkIterator, get_dimension

include("dispatchers.jl")
export Dispatcher, dispatch, dispatch_file, dispatch_to_learners
include("rpt_dispatcher.jl")
export RPTDispatcher
include("random_dispatcher.jl")
export RandomDispatcher
include("partition_tree_dispatcher.jl")
export PartitionTreeDispatcher
include("LSH_dispatcher.jl")
export LSHDispatcher, make_lsh_dispatcher

include("learners.jl")
include("learners2.jl")
include("disk_learner.jl")
export Learner, add_example!, add_examples!, train!, predict, LibLinClassifier, num_examples
export NLOptLearner, DiskLearner

include("SyntheticData.jl")
export SyntheticDistribution, SyntheticDataIterator, SyntheticDataChunkIterator
export sample

end
