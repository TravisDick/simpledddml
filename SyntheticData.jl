include("LinkageClustering.jl")

type SyntheticDistribution
    dim::Int
    centers::Array{Float64, 2}
    σ::Float64
    labels::Vector{Int}
end

function assign_labels!(rng, labels, n::LinkageLeaf, min_label, max_label)
    labels[n.v] = rand(min_label:max_label)
end

function assign_labels!(rng, labels, n::LinkageInternal,
                       min_label, max_label)
    if min_label == max_label
        assign_labels!(rng, labels, n.left, min_label, max_label)
        assign_labels!(rng, labels, n.right, min_label, max_label)
    else
        left_proportion = numleaves(n.left) / numleaves(n)
        num_labels = max_label - min_label
        midpoint = min_label + floor(Int, left_proportion * num_labels)
        assign_labels!(rng, labels, n.left, min_label, midpoint)
        assign_labels!(rng, labels, n.right, midpoint+1, max_label)
    end
end

function SyntheticDistribution(seed, N, σ, L, dim)
    rng = MersenneTwister(seed)
    X = rand(rng, dim, N)
    root = complete_linkage(X) #single_linkage(X)
    labels = Array(Int, N)
    assign_labels!(rng, labels, root, 1, L)
    # rng = MersenneTwister(seed)
    # X = Array(Float64, dim, N*L)
    # labels = collect(1:N*L)
    # ix = 1
    # for i in 1:N
    #     center = randn(rng, dim)
    #     center = center / norm(center)
    #     for j in 1:L
    #         offset = randn(rng, dim)
    #         offset = offset / norm(offset) / 10
    #         X[:,ix] = center + offset
    #         ix += 1
    #     end
    # end
    return SyntheticDistribution(dim, X, σ, labels)
end

function sample(d::SyntheticDistribution)
    ix = rand(1:size(d.centers,2))
    return (randn(d.dim) * d.σ + d.centers[:,ix], d.labels[ix])
end

function sample(d::SyntheticDistribution, n)
    X = Array(Float64, d.dim, n)
    y = Array(Int, n)
    for i in 1:n
        X[:,i], y[i] = sample(d)
    end
    return X, y
end

type SyntheticDataIterator
    d::SyntheticDistribution
    num_examples::Int
end

function SyntheticDataIterator(seed, N, σ, L, dim, num_examples)
    distribution = SyntheticDistribution(seed, N, σ, L, dim)
    return SyntheticDataIterator(distribution, num_examples)
end

get_dimension(i::SyntheticDataIterator) = i.d.dim

start(i::SyntheticDataIterator) = 0
next(i::SyntheticDataIterator, s) = (sample(i.d), s + 1)
done(i::SyntheticDataIterator, s) = s >= i.num_examples
eltype(i::SyntheticDataIterator) = Tuple{Vector{Float64}, Int}
length(i::SyntheticDataIterator) = i.num_examples

type SyntheticDataChunkIterator
    iter::SyntheticDataIterator
    num_chunks::Int
end

function SyntheticDataChunkIterator(seed,N,σ,L,dim, num_examples, num_chunks)
    iter = SyntheticDataIterator(seed, N, σ, L, dim, num_examples)
    return SyntheticDataChunkIterator(iter, num_chunks)
end

get_dimension(i::SyntheticDataChunkIterator) = get_dimension(i.iter)

start(i::SyntheticDataChunkIterator) = 0
next(i::SyntheticDataChunkIterator, s) = (i.iter, s + 1)
done(i::SyntheticDataChunkIterator, s) = s >= i.num_chunks
eltype(i::SyntheticDataChunkIterator) = SyntheticDataIterator
length(i::SyntheticDataChunkIterator) = i.num_chunks
