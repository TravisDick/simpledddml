using NLopt

type NLOptLearner
    path::ASCIIString # Path to the file that will store the data
    fh::IOStream
    dim::Int
    L::Int # Number of classes
    λ::Float64 # The regularization parameter
    model::Array{Float64, 2}
    num_examples::Int
end

function NLOptLearner(base_dir, dim, L, λ)
    path = joinpath(base_dir, "$(hash(rand())).dat")
    fh = open(path, "w")
    return NLOptLearner(path, fh, dim, L, λ, Array(Float64, dim+1, L), 0)
end

function add_example!(l::NLOptLearner, x, y)
    write(l.fh, y)
    write(l.fh, x)
    l.num_examples += 1
end

function add_examples!(l::NLOptLearner, Xs, ys)
    n = length(ys) # number of examples
    dim = div(length(Xs), n)
    Xs = reshape(Xs, dim, n)
    for i in 1:n
        write(l.fh, ys[i])
        write(l.fh, Xs[:,i])
    end
    l.num_examples += n
end

# function to efficiently compute dot product (with bias)
function mydot(w::Vector, x::Vector)
    dotp = 0.
    for i in 1:length(x)
        dotp += w[i] * x[i]
    end
    dotp += w[end]
    return dotp
end
# Assume classes are labeled 1..numClasses
# w: matrix of size (d+1) \times numClasses
#    w[:, j] is for class j vs others
#    last entry of w is the bias
function solve(filename, numClasses, dim, λ = 0.1)
    w = zeros(dim + 1, numClasses)
    file = open(filename)
    count = 0
    # function value and gradient
    function fg!(w::Vector, g::Vector, pos_label::Int)
        count += 1
        dim = length(w) - 1
        y = 0.
        x = zeros(dim)
        fval = 0.
        try
            if (length(g) > 0) # compute both gradient and function val
                g[:] = 0.
                b = 0.
                while(!eof(file))
                    y = (read(file, Int) == pos_label) ? 1.0 : -1.0
                    read!(file, x)
                    b = 1. - y * mydot(w, x)
                    if (b > 0)
                        fval += b^2
                        for j in 1:length(x)
                            g[j] += -2. * y * b * x[j]
                        end
                        g[end] += -2. * y * b
                    end
                end
                for i in 1:length(w)-1
                    fval += λ*w[i]*w[i]
                    g[i] += 2*λ*w[i]
                end
            else # compute only function val
                while(!eof(file))
                    y = (read(file, Int) == pos_label) ? 1.0 : -1.0
                    read!(file, x)
                    fval += (max(0, 1 - y * mydot(w, x)))^2
                end
                for i in 1:length(w)-1
                    fval += λ*w[i]*w[i]
                end
            end
            seekstart(file)
        catch ex
            println("fg! threw an exception!")
            @show ex
        end
        return fval
    end
    for i in 1:numClasses
        opt = Opt(:LD_LBFGS, dim+1)
        maxeval!(opt, 30)
        min_objective!(opt, (w, g) -> fg!(w, g, i)) #pass lambda with two args
        (min_f,min_w,ret) = optimize(opt, rand(dim + 1))
        w[:,i] = min_w
    end
    println("$count passes over the data")
    return w
end

function predict(w::Array{Float64, 2}, x::Vector)
    dp = mydot(w[:,1], x)
    dp1 = 0.
    y = 1
    for i in 2:size(w, 2)
        dp1 = mydot(w[:,i], x)
        if (dp1 > dp)
            dp = dp1
            y = i
        end
    end
    return y
end

function train!(l::NLOptLearner)
    close(l.fh)
    l.model = solve(l.path, l.L, l.dim, l.λ)
    rm(l.path)
end

function predict(l::NLOptLearner, x)
    return predict(l.model, x)
end

num_examples(l::NLOptLearner) = l.num_examples
