import Base.Collections: PriorityQueue, dequeue!
import Base: hash

type LSHDispatcher{H}
    h::H
    remap::Dict{Int,Int}
    k::Int
end

function LSHDispatcher{H}(X::Matrix, k::Int, h::H)
    # Count how many points land in each bin
    pq = PriorityQueue(Int, Int, Base.Order.Reverse)
    for i in 1:size(X,2)
        b = hash(h, vec(X[:,i]))
        pq[b] = get(pq, b, 0) + 1
    end
    # Set up the bin mapping so that the k fullest bins do not collide
    remap = Dict{Int,Int}()
    for i in 1:k
        if length(pq) == 0
            break
        end
        remap[dequeue!(pq)] = i
    end
    LSHDispatcher(h, remap, k)
end

cluster_keys(d::LSHDispatcher) = 1:d.k

function dispatch(d::LSHDispatcher, x::Vector)
    b = hash(d.h, x)
    get(d.remap, b, mod(b,d.k) + 1)
end

# A simple LSH function to use with the above LSH dispatcher

type ProjectionHash
    P::Matrix{Float64}
    width::Float64
    num_bins::Int
end

function ProjectionHash(dim::Int, m::Int, width::Number, num_bins::Int)
    # Pick a random projection matrix and normalize its rows to have unit length
    P = randn(m, dim)
    for i in 1:dim
        P[:,i] /= norm(P[:,i])
    end
    return ProjectionHash(P, width, num_bins)
end

function ProjectionHash(dim::Int, m::Int, k::Int, X::Matrix)
    num_bins = ceil(Int, k^(2/m))

    P = randn(m, dim)
    for i in 1:dim
        P[:,i] /= norm(P[:,i])
    end

    min_proj = Inf
    max_proj = -Inf
    for i in 1:m
        for j in 1:size(X,2)
            px = dot(vec(P[i,:]), vec(X[:,j]))
            min_proj = min(min_proj, px)
            max_proj = max(max_proj, px)
        end
    end

    width = max_proj - min_proj

    return ProjectionHash(P, width, num_bins)
end

function hash(h::ProjectionHash, x::Vector)
    bin = 0
    for i in 1:size(h.P, 1)
        bin *= h.num_bins
        Pxi = dot(vec(h.P[i,:]), x)
        bin += mod(floor(Int, Pxi/h.width*h.num_bins), h.num_bins)
    end
    bin
end

# A function for constructing the LSH dispatcher

function make_lsh_dispatcher(X::Matrix, k::Int, m=10)
    LSHDispatcher(X, k, ProjectionHash(size(X,1), m, k, X))
end
